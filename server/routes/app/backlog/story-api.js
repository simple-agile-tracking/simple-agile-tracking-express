const express = require('express');
const router = express.Router();

const story = require('./story');
const issue = require('./issue');

router.get('/:prodid', (req, res) => story.getAll(req, res));

router.get(
  '/:prodid/excel',
  (req, res, next) => story.getExportData(req, res, next),
  (req, res) => story.getExport(req, res)
);

router.get(
  '/:prodid/excelbyepic',
  (req, res, next) => story.getExportDataByEpic(req, res, next),
  (req, res) => story.getExportByEpic(req, res)
);

router.get('/:prodid/free', (req, res) => story.getFree(req, res));

router.get('/:prodid/sprint/:sprintid', (req, res) => story.getBySprint(req, res));

router.get(
  '/:prodid/:itemid',
  (req, res, next) => story.getById(req, res, next),
  (req, res, next) => issue.getAll(req, res, next),
  (req, res) => issue.sendAll(req, res)
);

router.get('/:prodid/:epicid/:featid', (req, res) => story.getByFeature(req, res));

module.exports = router;
