const express = require('express');
const router = express.Router();

const sprint = require('./sprint');

router.delete('/:prodid/:id/rem/:storyid', (req, res) => sprint.removeItem(req, res));

router.delete('/:prodid/:id', (req, res) => sprint.delete(req, res));

router.post('/:prodid/:id', (req, res) => sprint.addItem(req, res));

router.post('/:prodid', (req, res) => sprint.insert(req, res));

router.put('/:prodid/:id', (req, res) => sprint.update(req, res));

module.exports = router;
