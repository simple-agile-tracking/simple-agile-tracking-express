const SparkPost = require('sparkpost');

const mailer = {
  send: (req, res) => {
    const client = new SparkPost();

    client.transmissions.send({
      content: {
        from: req.app.get('sparkpostFrom'),
        subject: req.body.Subject,
        html: req.body.Message
      },
      recipients: [
        { address: req.body.EmailTo }
      ]
    })
      .then(() => res.status(200).send())
      .catch(err => res.status(400).send(err));
  }
}

module.exports = mailer;
