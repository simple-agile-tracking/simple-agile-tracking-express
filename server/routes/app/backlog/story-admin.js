const express = require('express');
const router = express.Router();

const story = require('./story');
const issue = require('./issue');
const auth = require('../../auth');

router.get('/users', (req, res) => auth.getAll(req, res));
router.get('/user/:uid', (req, res) => auth.getUser(req, res));

router.delete('/:prodid/:itemid/issue/:id', (req, res) => issue.delete(req, res));
router.delete('/:prodid/:id', (req, res) => story.delete(req, res));

router.post('/:prodid/:epicid/convert/:id', (req, res) => story.convert(req, res));
router.post('/:prodid/:itemid/issue', (req, res) => issue.insert(req, res));
router.post('/:prodid/:epicid/:featid', (req, res) => story.insert(req, res));

router.put('/:prodid/:itemid/issue/:id', (req, res) => issue.update(req, res));
router.put('/:prodid/:sprintid/story/:id', (req, res) => story.updateBySprint(req, res));
router.put('/:prodid/:epicid/:featid/:id', (req, res) => story.update(req, res));

module.exports = router;
