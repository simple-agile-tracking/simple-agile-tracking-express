const express = require('express');
const router = express.Router();

const release = require('./release');

router.get('/:prodid/:id', (req, res) => release.getById(req, res));

router.get('/:prodid', (req, res) => release.getAll(req, res));

module.exports = router;
