const express = require('express');
const router = express.Router();

const auth = require('./auth');
const logger = require('./logger');

router.get('/user', (req, res) => auth.getDBUser(req, res));

router.post('/log', (req, res) => logger.insert(req, res));
router.post('/user', (req, res) => auth.insert(req, res));

router.put('/user', (req, res) => auth.update(req, res));

module.exports = router;
