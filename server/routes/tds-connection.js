const tds = require('tedious');

module.exports = {
  delete: (req, res, qryStr, params) => {
    go(req, res, 'update', qryStr, params);
  },

  insert: (req, res, qryStr, params) => {
    go(req, res, 'insert', qryStr, params);
  },

  select: (req, res, qryStr, params, recordType, next) => {
    if (next) {
      goFirst(req, res, 'select', qryStr, params, recordType, next);
    } else {
      go(req, res, 'select', qryStr, params);
    }
  },

  selectOne: (req, res, qryStr, params, recordType, next) => {
    if (next) {
      goFirst(req, res, 'selectOne', qryStr, params, recordType, next);
    } else {
      go(req, res, 'selectOne', qryStr, params, recordType);
    }
  },

  update: (req, res, qryStr, params) => {
    go(req, res, 'update', qryStr, params);
  }
}

cleanView = (rows) => {
  // strip out all the tedious metadata and return just the column values
  const data = [];

  for (let r of rows) {
    const row = {};
    for (c in r) {
      row[c] = r[c].value;
    }
    data.push(row);
  }

  return data;
}

go = (req, res, verb, qryStr, params, recordType) => {
  const Connection = tds.Connection;
  const connection = new Connection(req.app.get('tdscfg'));
  const TYPES = tds.TYPES;

  connection.on('connect', e => {
    if (e) {
      // console.error(e);
      return res.status(500).send({ message: 'Database connection error.' });
    } else {
      const Request = tds.Request;
      let rtn = null;
      let status = verb === 'insert' ? 201 : 200;

      request = new Request(qryStr, (e, rowCount, rows) => {
        if (e) {
          rtn = { message: (e.number === 229 ? 'Database security error.' : 'Unknown database issue.') };
          status = e.number === 229 ? 400 : 500;
        }
        connection.close();
      });

      if (params) {
        for (let p of params) {
          if (!!p.length) {
            request.addParameter(p.name, TYPES[p.type], p.value, { length: p.length });
          } else {
            request.addParameter(p.name, TYPES[p.type], p.value);
          }
        }
      }

      if (verb === 'select') {
        request.on('doneInProc', (rowCount, more, rows) => rtn = cleanView(rows));
      } else if (verb === 'selectOne') {
        const type = recordType ? recordType : 'record';
        request.on('doneInProc', (rowCount, more, rows) => {
          if (rows.length === 0) {
            rtn = { message: 'Requested ' + type + ' could not be found.' };
            status = 404;
          } else {
            rtn = cleanView(rows)[0];
          }
        });
      }

      request.on('requestCompleted', () => res.status(status).send(rtn));

      connection.execSql(request);
    }
  });
}

goFirst = (req, res, verb, qryStr, params, recordType, next) => {
  const Connection = tds.Connection;
  const connection = new Connection(req.app.get('tdscfg'));
  const TYPES = tds.TYPES;
  let type = 'record';

  connection.on('connect', e => {
    if (e) {
      // console.error(e);
      return res.status(500).send({ message: 'Database connection error.' });
    } else {
      const Request = tds.Request;
      let rtn = null;
      let status = verb === 'insert' ? 201 : 200;
      let go = true;

      request = new Request(qryStr, (e, rowCount, rows) => {
        if (e) {
          rtn = { message: (e.number === 229 ? 'Database security error.' : 'Unknown database issue.') };
          status = e.number === 229 ? 400 : 500;
          go = false;
        }
        connection.close();
      });

      if (params) {
        for (let p of params) {
          if (!!p.length) {
            request.addParameter(p.name, TYPES[p.type], p.value, { length: p.length });
          } else {
            request.addParameter(p.name, TYPES[p.type], p.value);
          }
        }
      }

      if (verb === 'select') {
        type = recordType ? recordType : 'records';
        request.on('doneInProc', (rowCount, more, rows) => rtn = cleanView(rows));
      } else if (verb === 'selectOne') {
        type = recordType ? recordType : 'record';
        request.on('doneInProc', (rowCount, more, rows) => {
          if (rows.length === 0) {
            rtn = { message: 'Requested ' + type + ' could not be found.' };
            status = 404;
            go = false;
          } else {
            rtn = cleanView(rows)[0];
          }
        });
      }

      request.on('requestCompleted', () => {
        if (go) {
          res.locals['_' + type] = rtn;
          next();
        } else {
          res.status(status).send(rtn);
        }
      });

      connection.execSql(request);
    }
  });
}
