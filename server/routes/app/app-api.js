const express = require('express');
const router = express.Router();

const product = require('./product');

router.get('/start', (req, res) => product.getAll(req, res));

module.exports = router;
