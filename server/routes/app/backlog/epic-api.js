const express = require('express');
const router = express.Router();

const epic = require('./epic');

router.get(
  '/:prodid/excel',
  (req, res, next) => epic.getExportData(req, res, next),
  (req, res) => epic.getExport(req, res)
);

router.get('/:prodid/list', (req, res) => epic.getList(req, res));

router.get('/:prodid/:id', (req, res) => epic.getById(req, res));

router.get('/:prodid', (req, res) => epic.getAll(req, res));

module.exports = router;
