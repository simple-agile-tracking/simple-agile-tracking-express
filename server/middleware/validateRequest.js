const auth = require('../routes/auth');

module.exports = (req, res, next) => {
  const token = (req.body && req.body.token) || (req.query && req.query.token) || req.headers.token;
  const uname = (req.body && req.body.username) || (req.query && req.query.username) || req.headers.username;
  const key = (req.body && req.body.key) || (req.query && req.query.key) || req.headers.key;

  if (token || uname || key) {
    try {
      auth.validate(req, res, next, token, uname, key);
    } catch (err) {
      return res.status(500).send(err);
    }
  } else {
    return res.status(401).send({ message: 'Invalid token or key' });
  }
};
