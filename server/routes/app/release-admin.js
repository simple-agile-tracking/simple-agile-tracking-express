const express = require('express');
const router = express.Router();

const release = require('./release');

router.delete('/:prodid/:id/rem/:sprintid', (req, res) => release.removeFromSprint(req, res));

router.delete('/:prodid/:id', (req, res) => release.delete(req, res));

router.post('/:prodid/:id', (req, res) => release.addToSprint(req, res));

router.post('/:prodid', (req, res) => release.insert(req, res));

router.put('/:prodid/:id', (req, res) => release.update(req, res));

module.exports = router;
