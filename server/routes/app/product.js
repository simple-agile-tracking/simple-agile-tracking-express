const product = {
  getAll: (req, res) => {
    const tds = require('../tds-connection');
    const qryStr = `
      select Id,
        Name,
        Description,
        SprintLength,
        InBacklogPhase
      from AgileProduct
      where AuthGroupId = @agid
      order by Name
    `;
    const params = [
      { name: 'agid', type: 'NVarChar', value: req.app.get('agid'), length: 35 }
    ];
    
    tds.select(req, res, qryStr, params);
  },

  getById: (req, res) => {
    const tds = require('../tds-connection');
    const qryStr = `
      select Id,
        AuthGroupId,
        Name,
        Description,
        SprintLength,
        InBacklogPhase,
        DateCreated,
        OwnerId,
        LastModified
      from AgileProduct
      where Id = @id
        and AuthGroupId = @agid
    `;
    const params = [
      { name: 'agid', type: 'NVarChar', value: req.app.get('agid'), length: 35 },
      { name: 'id', type: 'NVarChar', value: req.params.id, length: 35 }
    ];
    
    tds.selectOne(req, res, qryStr, params, 'product');
  },

  delete: (req, res) => {
    // const tds = require('../tds-connection');
    const qryStr = `
      delete from AgileProduct
      where Id = @id
        and AuthGroupId = @agid
    `;
    const paramws = [
      { name: 'agid', type: 'NVarChar', value: req.app.get('agid'), length: 35 },
      { name: 'id', type: 'NVarChar', value: req.params.prodid, length: 35 }
    ];
    
    // tds.delete(req, res, qryStr, params);

    console.log('Deleting products is currently disabled.');
    return res.status(403).send({ message: 'Deleting products is currently disabled' });
  },

  insert: (req, res) => {
    const tds = require('../tds-connection');
    const qryStr = `
      insert into AgileProduct (
        Id,
        AuthGroupId,
        Name,
        Description,
        SprintLength,
        InBacklogPhase,
        DateCreated,
        OwnerId,
        LastModified
      ) values (
        @id,
        @agid,
        @name,
        @desc,
        @len,
        @phs,
        @dt,
        @usr,
        @dt
      )
    `;
    const params = [
      { name: 'agid', type: 'NVarChar', value: req.app.get('agid'), length: 35 },
      { name: 'id', type: 'NVarChar', value: req.body.Id, length: 35 },
      { name: 'name', type: 'NVarChar', value: req.body.Name, length: 50 },
      { name: 'desc', type: 'NVarChar', value: req.body.Description, length: 4001 },
      { name: 'len', type: SmallInt, value: req.body.SprintLength },
      { name: 'phs', type: 'Bit', value: req.body.InBacklogPhase },
      { name: 'dt', type: 'DateTime', value: new Date() },
      { name: 'usr', type: 'NVarChar', value: res.locals._current_user_id, length: 35 }
    ];
    
    tds.insert(req, res, qryStr, params);
  },

  update: (req, res) => {
    const tds = require('../tds-connection');
    const qryStr = `
      update AgileProduct
      set Name = @name,
        Description = @desc,
        SprintLength = @len,
        InBacklogPhase = @phs,
        OwnerId = @usr,
        LastModified = @dt
      where Id = @id
        and AuthGroupId = @agid
    `;
    const params = [
      { name: 'agid', type: 'NVarChar', value: req.app.get('agid'), length: 35 },
      { name: 'id', type: 'NVarChar', value: req.body.Id, length: 35 },
      { name: 'name', type: 'NVarChar', value: req.body.Name, length: 50 },
      { name: 'desc', type: 'NVarChar', value: req.body.Description, length: 4001 },
      { name: 'len', type: SmallInt, value: req.body.SprintLength },
      { name: 'phs', type: 'Bit', value: req.body.InBacklogPhase },
      { name: 'usr', type: 'NVarChar', value: res.locals._current_user_id, length: 35 },
      { name: 'dt', type: 'DateTime', value: new Date() }
    ];
    
    tds.update(req, res, qryStr, params);
  }
};

module.exports = product;
