const auth = {
  getAll: (req, res) => {
    const fireAdmin = req.app.get('fireAdmin');

    fireAdmin.auth().listUsers(100)
      .then(result => res.status(200).send(result.users))
      .catch(e => res.status(500).send({ message: e }));
  },

  getDBUser: (req, res) => {
    const tds = require('./tds-connection');
    const qryStr = `
      select m.Id,
        m.GroupAdmin,
        u.Id as UserId,
        u.DateBanned
      from NodeMap m
        inner join NodeUser u on m.UserId = u.Id
      where u.Email = @eml
        and m.GroupId = @gid
        and u.DateDeleted is null
    `;
    const params = [
      { name: 'eml', type: 'NVarChar', value: res.locals._current_user_name, length: 75 },
      { name: 'gid', type: 'NVarChar', value: req.app.get('applicationKey'), length: 35 }
    ];

    tds.selectOne(req, res, qryStr, params, 'user');
  },

  getUser: (req, res) => {
    const fireAdmin = req.app.get('fireAdmin');

    fireAdmin.auth().getUser(req.params.uid)
      .then(result => res.status(200).send(result))
      .catch(e => res.status(500).send({ message: e }));
  },

  insert: (req, res) => {
    const tds = require('./tds-connection');
    const qryStr = `
      insert into NodeUser (
        Id,
        FirstName,
        LastName,
        ScreenName,
        Email,
        SelfRegistered,
        DateCreated,
        DateModified,
        LastModifiedBy
      ) values (
        @id,
        @fn,
        @ln,
        @sn,
        @eml,
        @sreg,
        @dt,
        @dt,
        @id
      );

      insert into NodeMap (
        Id,
        UserId,
        GroupId,
        GroupAdmin,
        DateCreated,
        DateModified,
        LastModifiedBy
      ) values (
        @mid,
        @id,
        @gid,
        0,
        @dt,
        @dt,
        @id
      );
    `;
    const params = [
      { name: 'id', type: 'NVarChar', value: req.body.Id, length: 35 },
      { name: 'fn', type: 'NVarChar', value: req.body.FirstName, length: 50 },
      { name: 'ln', type: 'NVarChar', value: req.body.LastName, length: 50 },
      { name: 'sn', type: 'NVarChar', value: req.body.ScreenName, length: 50 },
      { name: 'eml', type: 'NVarChar', value: req.body.Email, length: 75 },
      { name: 'sreg', type: 'Bit', value: req.body.SelfRegistered || false },
      { name: 'dt', type: 'DateTime', value: new Date() },
      { name: 'mid', type: 'NVarChar', value: req.body.MapId, length: 35 },
      { name: 'gid', type: 'NVarChar', value: req.app.get('applicationKey'), length: 35 }
    ];
    
    tds.insert(req, res, qryStr, params);
  },

  update: (req, res) => {
    const tds = require('./tds-connection');
    const qryStr = `
      update NodeUser
      set ScreenName = @sn,
        DateModified = @dt,
        LastModifiedBy = @id
      where Id = @id
        and Id = (
          select UserId
          from NodeMap
          where UserId = @id
            and GroupId = @gid
        )
    `;
    const params = [
      { name: 'id', type: 'NVarChar', value: res.locals._current_user_id, length: 35 },
      { name: 'sn', type: 'NVarChar', value: req.body.ScreenName, length: 50 },
      { name: 'dt', type: 'DateTime', value: new Date() },
      { name: 'gid', type: 'NVarChar', value: req.app.get('applicationKey'), length: 35 }
    ];
    
    tds.update(req, res, qryStr, params);
  },

  validate: (req, res, next, token, username, userid) => {
    const tok = token || '';
    const un = username || '';
    const id = userid || '';

    if (tok === '' || un === '' || id === '') {
      return res.status(401).send({ message: 'Missing credentials' });
    } else {
      const fireAdmin = req.app.get('fireAdmin');

      fireAdmin.auth().verifyIdToken(tok)
        .then(decodedToken => {
          if (id === decodedToken.uid) {
            res.locals._current_user_name = un;
            res.locals._current_user_id = id;
            next();
          } else {
            return res.status(403).send({ message: 'Not authorized' });
          }
        }).catch(e => {
          // console.log(e);
          return res.status(401).send({ message: 'Invalid user' });
        });
    }
  }
};

module.exports = auth;
