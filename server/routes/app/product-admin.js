const express = require('express');
const router = express.Router();

const product = require('./product');

router.delete('/:prodid', (req, res) => product.delete(req, res));

router.post('', (req, res) => product.insert(req, res));

router.put('/:prodid', (req, res) => product.update(req, res));

module.exports = router;
