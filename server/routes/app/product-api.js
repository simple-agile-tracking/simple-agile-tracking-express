const express = require('express');
const router = express.Router();

const product = require('./product');

router.get('/:id', (req, res) => product.getById(req, res));

module.exports = router;
