const obj = {
  getAll: (req, res, next) => {
    const tds = require('../../tds-connection');
    const qryStr = `
      select i.Id,
        i.StoryId,
        i.Name,
        i.Description,
        i.AssignedTo,
        i.Status,
        i.DateCreated,
        i.OwnerId,
        i.LastModified
      from AgileIssue i
        inner join AgileStory s on i.StoryId = s.Id
        inner join AgileProduct p on s.ProductId = p.Id
      where i.StoryId = @itemid
        and p.AuthGroupId = @agid
      order by i.Name
    `;
    const params = [
      { name: 'agid', type: 'NVarChar', value: req.app.get('agid'), length: 35 },
      { name: 'itemid', type: 'NVarChar', value: req.params.itemid, length: 35 }
    ];

    tds.select(req, res, qryStr, params, 'issues', next);
  },

  sendAll: (req, res) => {
    return res.status(200).send({ Story: res.locals._story, Issues: res.locals._issues});
  },

  delete: (req, res) => {
    const tds = require('../../tds-connection');
    const qryStr = `
      delete from AgileIssue
      where Id = @id
        and StoryId = @itemid
    `;
    const params = [
      { name: 'id', type: 'NVarChar', value: req.params.id, length: 35 },
      { name: 'itemid', type: 'NVarChar', value: req.params.itemid, length: 35 }
    ];
    
    tds.delete(req, res, qryStr, params);
  },

  insert: (req, res) => {
    const tds = require('../../tds-connection');
    const qryStr = `
      insert into AgileIssue (
        Id,
        StoryId,
        Name,
        Description,
        AssignedTo,
        Status,
        DateCreated,
        OwnerId,
        LastModified
      ) values (
        @id,
        @itemid,
        @nm,
        @desc,
        @ato,
        @stat,
        @dt,
        @usr,
        @dt
      )
    `;
    const params = [
      { name: 'id', type: 'NVarChar', value: req.body.Id, length: 35 },
      { name: 'itemid', type: 'NVarChar', value: req.params.itemid, length: 35 },
      { name: 'nm', type: 'NVarChar', value: req.body.Name, length: 50 },
      { name: 'desc', type: 'NVarChar', value: req.body.Description, length: 4001 },
      { name: 'ato', type: 'NVarChar', value: req.body.AssignedTo, length: 35 },
      { name: 'stat', type: 'NVarChar', value: req.body.Status, length: 50 },
      { name: 'dt', type: 'DateTime', value: new Date() },
      { name: 'usr', type: 'NVarChar', value: res.locals._current_user_id, length: 35 }
    ];
    
    tds.insert(req, res, qryStr, params);
  },

  update: (req, res) => {
    const tds = require('../../tds-connection');
    const qryStr = `
      update AgileIssue
      set Name = @nm,
        Description = @desc,
        AssignedTo = @ato,
        Status = @stat,
        OwnerId = @usr,
        LastModified = @dt
      where Id = @id
        and StoryId = @itemid
    `;
    const params = [
      { name: 'id', type: 'NVarChar', value: req.body.Id, length: 35 },
      { name: 'itemid', type: 'NVarChar', value: req.params.itemid, length: 35 },
      { name: 'nm', type: 'NVarChar', value: req.body.Name, length: 50 },
      { name: 'desc', type: 'NVarChar', value: req.body.Description, length: 4001 },
      { name: 'ato', type: 'NVarChar', value: req.body.AssignedTo, length: 35 },
      { name: 'stat', type: 'NVarChar', value: req.body.Status, length: 50 },
      { name: 'usr', type: 'NVarChar', value: res.locals._current_user_id, length: 35 },
      { name: 'dt', type: 'DateTime', value: new Date() }
    ];
    
    tds.update(req, res, qryStr, params);
  }
};

module.exports = obj;
