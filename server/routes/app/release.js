const release = {
  getAll: (req, res) => {
    const tds = require('../tds-connection');
    const qryStr = `
      select r.Id,
        r.ReleaseNumber,
        r.ReleaseDate,
        r.Notes
      from AgileProduct p
        inner join AgileRelease r on p.Id = r.ProductId
      where p.Id = @prodid
        and p.AuthGroupId = @agid
      order by r.ReleaseNumber desc
    `;
    const params = [
      { name: 'agid', type: 'NVarChar', value: req.app.get('agid'), length: 35 },
      { name: 'prodid', type: 'NVarChar', value: req.params.prodid, length: 35 }
    ];
    
    tds.select(req, res, qryStr, params);
  },

  getById: (req, res) => {
    const tds = require('../tds-connection');
    const qryStr = `
      select r.Id,
          r.ProductId,
          r.ReleaseNumber,
          r.ReleaseDate,
          r.Notes,
          r.DateCreated,
          r.OwnerId,
          r.LastModified
      from AgileProduct p
        inner join AgileRelease r on p.Id = r.ProductId
      where r.Id = @id
        and p.Id = @prodid
        and p.AuthGroupId = @agid
    `;
    const params = [
      { name: 'agid', type: 'NVarChar', value: req.app.get('agid'), length: 35 },
      { name: 'id', type: 'NVarChar', value: req.params.id, length: 35 },
      { name: 'prodid', type: 'NVarChar', value: req.params.prodid, length: 35 }
    ];

    tds.selectOne(req, res, qryStr, params, 'release');
  },

  addToSprint: (req, res) => {
    const tds = require('../tds-connection');
    const qryStr = `
      update AgileSprint
      set ReleaseId = @id,
        OwnerId = @usr,
        LastModified = @dt
      where Id = @spt
        and ProductId = @prodid
        and (
          ReleaseId is null
          or ReleaseId = ''
        )
    `;
    const params = [
      { name: 'id', type: 'NVarChar', value: req.params.id, length: 35 },
      { name: 'prodid', type: 'NVarChar', value: req.params.prodid, length: 35 },
      { name: 'spt', type: 'NVarChar', value: req.body.SprintId, length: 35 },
      { name: 'usr', type: 'NVarChar', value: res.locals._current_user_id, length: 35 },
      { name: 'dt', type: 'DateTime', value: new Date() }
    ];
    
    tds.update(req, res, qryStr, params);
  },

  delete: (req, res) => {
    const tds = require('../tds-connection');
    const qryStr = `
      delete from AgileRelease
      where Id = @id
        and ProductId = @prodid
    `;
    const params = [
      { name: 'id', type: 'NVarChar', value: req.params.id, length: 35 },
      { name: 'prodid', type: 'NVarChar', value: req.params.prodid, length: 35 }
    ];
    
    tds.delete(req, res, qryStr, params);
  },

  insert: (req, res) => {
    const tds = require('../tds-connection');
    const qryStr = `
      insert into AgileRelease (
        Id,
        ProductId,
        ReleaseNumber,
        ReleaseDate,
        Notes,
        DateCreated,
        OwnerId,
        LastModified
      ) values (
        @id,
        @prodid,
        @cnt,
        @rdt,
        @note,
        @dt,
        @usr,
        @dt
      )
    `;
    const params = [
      { name: 'id', type: 'NVarChar', value: req.body.Id, length: 35 },
      { name: 'prodid', type: 'NVarChar', value: req.params.prodid, length: 35 },
      { name: 'cnt', type: 'Int', value: req.body.ReleaseNumber },
      { name: 'rdt', type: 'Date', value: new Date(req.body.ReleaseDate) },
      { name: 'note', type: 'NVarChar', value: req.body.Notes, length: 4001 },
      { name: 'dt', type: 'DateTime', value: new Date() },
      { name: 'usr', type: 'NVarChar', value: res.locals._current_user_id, length: 35 }
    ];
    
    tds.insert(req, res, qryStr, params);
  },

  removeFromSprint: (req, res) => {
    const tds = require('../tds-connection');
    const qryStr = `
      update AgileSprint
      set ReleaseId = null,
        OwnerId = @usr,
        LastModified = @dt
      where Id = @spt
        and ReleaseId = @id
        and ProductId = @prodid
    `;
    const params = [
      { name: 'id', type: 'NVarChar', value: req.params.id, length: 35 },
      { name: 'prodid', type: 'NVarChar', value: req.params.prodid, length: 35 },
      { name: 'spt', type: 'NVarChar', value: req.params.sprintid, length: 35 },
      { name: 'usr', type: 'NVarChar', value: res.locals._current_user_id, length: 35 },
      { name: 'dt', type: 'DateTime', value: new Date() }
    ];
    
    tds.update(req, res, qryStr, params);
  },

  update: (req, res) => {
    const tds = require('../tds-connection');
    const qryStr = `
      update AgileRelease
      set ReleaseNumber = @cnt,
        ReleaseDate = @rdt,
        Notes = @note,
        OwnerId = @usr,
        LastModified = @dt
      where Id = @id
        and ProductId = @prodid
    `;
    const params = [
      { name: 'id', type: 'NVarChar', value: req.params.id, length: 35 },
      { name: 'prodid', type: 'NVarChar', value: req.params.prodid, length: 35 },
      { name: 'cnt', type: 'Int', value: req.body.ReleaseNumber },
      { name: 'rdt', type: 'Date', value: new Date(req.body.ReleaseDate) },
      { name: 'note', type: 'NVarChar', value: req.body.Notes, length: 4001 },
      { name: 'usr', type: 'NVarChar', value: res.locals._current_user_id, length: 35 },
      { name: 'dt', type: 'DateTime', value: new Date() }
    ];
    
    tds.update(req, res, qryStr, params);
  }
};

module.exports = release;
