const logger = {
  insert: (req, res) => {
    const loggingOn = !!(req.app.get('logging') === 'on');

    if (loggingOn) {
      const tds = require('./tds-connection');
      const qryStr = `
        insert into ApplicationLog (
          Id,
          ApplicationKey,
          UserKey,
          UserEmail,
          Path,
          ActivityDate
        ) values (
          @id,
          @gid,
          @uk,
          @eml,
          @path,
          @dt
        )
      `;
      const params = [
        { name: 'id', type: 'NVarChar', value: req.body.Id, length: 35 },
        { name: 'gid', type: 'NVarChar', value: req.app.get('applicationKey'), length: 35 },
        { name: 'uk', type: 'NVarChar', value: res.locals._current_user_id, length: 128 },
        { name: 'eml', type: 'NVarChar', value: res.locals._current_user_name, length: 75 },
        { name: 'path', type: 'NVarChar', value: req.body.Path, length: 50 },
        { name: 'dt', type: 'DateTime', value: new Date() }
      ];
      
      tds.insert(req, res, qryStr, params);
    } else {
      return res.status(200).send();
    }
  }
};

module.exports = logger;
