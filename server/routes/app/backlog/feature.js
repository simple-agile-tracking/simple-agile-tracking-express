const feature = {
  getAll: (req, res) => {
    const tds = require('../../tds-connection');
    const qryStr = `
      select f.Id,
        f.Title,
        f.Description,
        f.RisksDependencies,
        f.Priority,
        (
          select count(*)
          from AgileStory
          where FeatureId = f.Id
            and ProductId = p.Id
        ) as Items
      from AgileFeature f
        inner join AgileEpic e on f.EpicId = e.Id
        inner join AgileProduct p on e.ProductId = p.Id
      where e.Id = @epicid
        and p.Id = @prodid
        and p.AuthGroupId = @agid
      order by case when f.Priority is null then 1 else 0 end, f.Priority, f.Title
    `;
    const params = [
      { name: 'agid', type: 'NVarChar', value: req.app.get('agid'), length: 35 },
      { name: 'epicid', type: 'NVarChar', value: req.params.epicid, length: 35 },
      { name: 'prodid', type: 'NVarChar', value: req.params.prodid, length: 35 }
    ];
    
    tds.select(req, res, qryStr, params);
  },

  getById: (req, res) => {
    const tds = require('../../tds-connection');
    const qryStr = `
      select f.Id,
        f.EpicId,
        f.Title,
        f.Description,
        f.RisksDependencies,
        f.Priority,
        f.DateCreated,
        f.OwnerId,
        f.LastModified
      from AgileFeature f
        inner join AgileEpic e on f.EpicId = e.Id
        inner join AgileProduct p on e.ProductId = p.Id
      where f.Id = @featid
        and e.Id = @epicid
        and p.Id = @prodid
        and p.AuthGroupId = @agid
    `;
    const params = [
      { name: 'agid', type: 'NVarChar', value: req.app.get('agid'), length: 35 },
      { name: 'epicid', type: 'NVarChar', value: req.params.epicid, length: 35 },
      { name: 'prodid', type: 'NVarChar', value: req.params.prodid, length: 35 },
      { name: 'featid', type: 'NVarChar', value: req.params.featid, length: 35 }
    ];
    
    tds.selectOne(req, res, qryStr, params, 'feature');
  },

  getExportData: (req, res, next) => {
    const tds = require('../../tds-connection');
    const qryStr = `
      select f.Title,
        f.Description,
        f.RisksDependencies,
        f.Priority,
        e.Title as EpicTitle,
        e.Priority as EpicPriority
      from AgileFeature f
        inner join AgileEpic e on f.EpicId = e.Id
        inner join AgileProduct p on e.ProductId = p.Id
      where p.Id = @prodid
        and p.AuthGroupId = @agid
      order by f.Title, e.Priority, case when f.Priority is null then 1 else 0 end, f.Priority
    `;
    const params = [
      { name: 'agid', type: 'NVarChar', value: req.app.get('agid'), length: 35 },
      { name: 'prodid', type: 'NVarChar', value: req.params.prodid, length: 35 }
    ];
    
    tds.select(req, res, qryStr, params, 'features', next);
  },

  getExport: (req, res) => {
    const excel = require('node-excel-export');
    const styles = { hdr: { font: { bold: true } } };
    const spec = {
      Title: {
        displayName: 'Feature',
        headerStyle: styles.hdr,
        width: '20'
      },
      Description: {
        displayName: 'Description',
        headerStyle: styles.hdr,
        width: '50'
      },
      RisksDependencies: {
        displayName: 'Risks and Dependencies',
        headerStyle: styles.hdr,
        width: '50'
      },
      Priority: {
        displayName: 'Priority',
        headerStyle: styles.hdr,
        width: '10'
      },
      EpicTitle: {
        displayName: 'Epic',
        headerStyle: styles.hdr,
        width: '20'
      },
      EpicPriority: {
        displayName: 'Epic Priority',
        headerStyle: styles.hdr,
        width: '10'
      }
    };
    const data = res.locals._features;
    const rpt = excel.buildExport(
      [
        {
          name: 'Features',
          specification: spec,
          data: data
        }
      ]
    );

    res.attachment('features.xlsx');

    return res.status(200).send(rpt);
  },

  getExportDataByEpic: (req, res, next) => {
    const tds = require('../../tds-connection');
    const qryStr = `
      select e.Title as EpicTitle,
        e.Priority as EpicPriority,
        f.Title,
        f.Description,
        f.RisksDependencies,
        f.Priority
      from AgileFeature f
        inner join AgileEpic e on f.EpicId = e.Id
        inner join AgileProduct p on e.ProductId = p.Id
      where p.Id = @prodid
        and p.AuthGroupId = @agid
      order by e.Priority, case when f.Priority is null then 1 else 0 end, f.Priority, f.Title
    `;
    const params = [
      { name: 'agid', type: 'NVarChar', value: req.app.get('agid'), length: 35 },
      { name: 'prodid', type: 'NVarChar', value: req.params.prodid, length: 35 }
    ];
    
    tds.select(req, res, qryStr, params, 'features', next);
  },

  getExportByEpic: (req, res) => {
    const excel = require('node-excel-export');
    const styles = { hdr: { font: { bold: true } } };
    const spec = {
      EpicTitle: {
        displayName: 'Epic',
        headerStyle: styles.hdr,
        width: '20'
      },
      EpicPriority: {
        displayName: 'Epic Priority',
        headerStyle: styles.hdr,
        width: '10'
      },
      Title: {
        displayName: 'Feature',
        headerStyle: styles.hdr,
        width: '20'
      },
      Description: {
        displayName: 'Description',
        headerStyle: styles.hdr,
        width: '50'
      },
      RisksDependencies: {
        displayName: 'Risks and Dependencies',
        headerStyle: styles.hdr,
        width: '50'
      },
      Priority: {
        displayName: 'Priority',
        headerStyle: styles.hdr,
        width: '10'
      }
    };
    const data = res.locals._features;
    const rpt = excel.buildExport(
      [
        {
          name: 'Features by Epic',
          specification: spec,
          data: data
        }
      ]
    );

    res.attachment('featuresByEpic.xlsx');

    return res.status(200).send(rpt);
  },

  getList: (req, res) => {
    const tds = require('../../tds-connection');
    const qryStr = `
      select f.Id,
        f.EpicId,
        f.Title
      from AgileFeature f
        inner join AgileEpic e on f.EpicId = e.Id
        inner joing AgileProduct p on e.ProductId = p.Id
      where e.Id = @epicid
        and p.Id = @prodid
        and p.AuthGroupId = @agid
      order by f.Title
    `;
    const params = [
      { name: 'agid', type: 'NVarChar', value: req.app.get('agid'), length: 35 },
      { name: 'prodid', type: 'NVarChar', value: req.params.prodid, length: 35 },
      { name: 'epicid', type: 'NVarChar', value: req.params.epicid, length: 35 }
    ];
    
    tds.select(req, res, qryStr, params);
  },

  convert: (req, res) => {
    const tds = require('../../tds-connection');
    const qryStr = `
      insert into AgileFeature (
        Id,
        EpicId,
        Title,
        Description,
        RisksDependencies,
        Priority,
        DateCreated,
        OwnerId,
        LastModified
      ) values (
        @id,
        @epicid,
        @ttl,
        @desc,
        @rds,
        @pri,
        @dt,
        @usr,
        @dt
      );

      delete from AgileStory
      where Id = @id;
    `;
    const params = [
      { name: 'id', type: 'NVarChar', value: req.params.id, length: 35 },
      { name: 'epicid', type: 'NVarChar', value: req.body.EpicId, length: 35 },
      { name: 'ttl', type: 'NVarChar', value: req.body.Title, length: 50 },
      { name: 'desc', type: 'NVarChar', value: req.body.Description, length: 4001 },
      { name: 'rds', type: 'NVarChar', value: req.body.RisksDependencies, length: 4001 },
      { name: 'pri', type: 'Int', value: req.body.Priority || null },
      { name: 'dt', type: 'DateTime', value: new Date() },
      { name: 'usr', type: 'NVarChar', value: res.locals._current_user_id, length: 35 }
    ];
    
    tds.insert(req, res, qryStr, params);
  },

  delete: (req, res) => {
    const tds = require('../../tds-connection');
    const qryStr = `
      delete from AgileFeature
      where Id = @id
        and EpicId = @epicid
    `;
    const params = [
      { name: 'id', type: 'NVarChar', value: req.params.id, length: 35 },
      { name: 'epicid', type: 'NVarChar', value: req.params.epicid, length: 35 }
    ];
    
    tds.delete(req, res, qryStr, params);
  },

  insert: (req, res) => {
    const tds = require('../../tds-connection');
    const qryStr = `
      insert into AgileFeature (
        Id,
        EpicId,
        Title,
        Description,
        RisksDependencies,
        Priority,
        DateCreated,
        OwnerId,
        LastModified
      ) values (
        @id,
        @epicid,
        @ttl,
        @desc,
        @rds,
        @pri,
        @dt,
        @usr,
        @dt
      )
    `;
    const params = [
      { name: 'id', type: 'NVarChar', value: req.body.Id, length: 35 },
      { name: 'epicid', type: 'NVarChar', value: req.params.epicid, length: 35 },
      { name: 'ttl', type: 'NVarChar', value: req.body.Title, length: 50 },
      { name: 'desc', type: 'NVarChar', value: req.body.Description, length: 4001 },
      { name: 'rds', type: 'NVarChar', value: req.body.RisksDependencies, length: 4001 },
      { name: 'pri', type: 'Int', value: req.body.Priority || null },
      { name: 'dt', type: 'DateTime', value: new Date() },
      { name: 'usr', type: 'NVarChar', value: res.locals._current_user_id, length: 35 }
    ];
    
    tds.insert(req, res, qryStr, params);
  },

  update: (req, res) => {
    const tds = require('../../tds-connection');
    const qryStr = `
      update AgileFeature
      set EpicId = @newepic,
        Title = @ttl,
        Description = @desc,
        RisksDependencies = @rds,
        Priority = @pri,
        OwnerId = @usr,
        LastModified = @dt
      where Id = @id
        and EpicId = @oldepic
    `;
    const params = [
      { name: 'id', type: 'NVarChar', value: req.body.Id, length: 35 },
      { name: 'oldepic', type: 'NVarChar', value: req.params.epicid, length: 35 },
      { name: 'newepic', type: 'NVarChar', value: req.body.EpicId, length: 35 },
      { name: 'ttl', type: 'NVarChar', value: req.body.Title, length: 50 },
      { name: 'desc', type: 'NVarChar', value: req.body.Description, length: 4001 },
      { name: 'rds', type: 'NVarChar', value: req.body.RisksDependencies, length: 4001 },
      { name: 'pri', type: 'Int', value: req.body.Priority || null },
      { name: 'usr', type: 'NVarChar', value: res.locals._current_user_id, length: 35 },
      { name: 'dt', type: 'DateTime', value: new Date() }
    ];
    
    tds.update(req, res, qryStr, params);
  }
};

module.exports = feature;
