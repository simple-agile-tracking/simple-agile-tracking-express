const express = require('express');
const path = require('path');
const http = require('http');
const bodyParser = require('body-parser');
const cors = require('cors');
const fireAdmin = require('firebase-admin');

const app = express();

const port = process.env.PORT || 3000;

if (process.env.NODE_ENV !== 'production') {
  require('dotenv').config();
}

app.set('applicationKey', 'SAT');
app.set('logging', process.env.LOGGING);
app.set('sparkpostFrom', process.env.SPARKPOST_FROM);
app.set(
  'tdscfg',
  {
    server: process.env.TDS_SERVER,
    authentication: {
      type: 'default',
      options: {
        userName: process.env.TDS_USERNAME,
        password: process.env.TDS_PASSWORD
      }
    },
    options: {
      database: process.env.TDS_DATABASE,
      rowCollectionOnDone: true,
      useColumnNames: true,
      encrypt: false
    }
  }
);
app.set('agid', process.env.DEFAULT_GROUP);
const fbaAcct = {
  "type": "service_account",
  "project_id": process.env.FIREBASE_PROJECT_ID,
  "private_key_id": process.env.FIREBASE_PRIVATE_KEY_ID,
  "private_key": process.env.FIREBASE_PRIVATE_KEY.replace(/\\n/g, '\n'),
  "client_email": process.env.FIREBASE_CLIENT_EMAIL,
  "client_id": process.env.FIREBASE_CLIENT_ID,
  "auth_uri": "https://accounts.google.com/o/oauth2/auth",
  "token_uri": "https://accounts.google.com/o/oauth2/token",
  "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
  "client_x509_cert_url": process.env.FIREBASE_CLIENT_CERT_URL
};
fireAdmin.initializeApp({
  credential: fireAdmin.credential.cert(fbaAcct),
  databaseURL: process.env.FIREBASE_DATABASE
});
app.set('fireAdmin', fireAdmin);

const corsOptions = {
  origin: [ process.env.SERVICE_PATH, process.env.APP_PATH ]
};
// dunno if this is still valid with the CORS crap below, but I think so ...
app.use(cors(corsOptions));
// CORS controls from the security blog post
app.all('/*', (req, res, next) => {
  // CORS headers
  res.header("Access-Control-Allow-Origin", "*"); // restrict all access to the required domains
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
  // set custom headers for CORS
  res.header('Access-Control-Allow-Headers', 'Content-Type,Accept,X-Access-Token,X-Key');
  if (req.method === 'OPTIONS') {
    res.status(200).end();
  } else {
    next();
  }
});

// parsers for POST data
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

// point static path to 'pub' folder
app.use(express.static(path.join(__dirname, 'pub')));

// set up security middleware
app.all('/api/app/*', [require('./server/middleware/validateRequest')]);
app.all('/api/core/*', [require('./server/middleware/validateRequest')]);

// get API routes
const api = require('./server/routes/api');
const coreAPI = require('./server/routes/core-api');
const appAPI = require('./server/routes/app/app-api');
const epicAPI = require('./server/routes/app/backlog/epic-api');
const epicAdminAPI = require('./server/routes/app/backlog/epic-admin');
const featureAPI = require('./server/routes/app/backlog/feature-api');
const featureAdminAPI = require('./server/routes/app/backlog/feature-admin');
const storyAPI = require('./server/routes/app/backlog/story-api');
const storyAdminAPI = require('./server/routes/app/backlog/story-admin');
const productAPI = require('./server/routes/app/product-api');
const productAdminAPI = require('./server/routes/app/product-admin');
const releaseAPI = require('./server/routes/app/release-api');
const releaseAdminAPI = require('./server/routes/app/release-admin');
const sprintAPI = require('./server/routes/app/sprint-api');
const sprintAdminAPI = require('./server/routes/app/sprint-admin');

// implement API routes
app.use('/api/app/epics/admin', epicAdminAPI);
app.use('/api/app/features/admin', featureAdminAPI);
app.use('/api/app/products/admin', productAdminAPI);
app.use('/api/app/releases/admin', releaseAdminAPI);
app.use('/api/app/sprints/admin', sprintAdminAPI);
app.use('/api/app/stories/admin', storyAdminAPI);
app.use('/api/app/epics', epicAPI);
app.use('/api/app/features', featureAPI);
app.use('/api/app/products', productAPI);
app.use('/api/app/releases', releaseAPI);
app.use('/api/app/sprints', sprintAPI);
app.use('/api/app/stories', storyAPI);
app.use('/api/app', appAPI);
app.use('/api/core', coreAPI);
app.use('/api', api);

// catch all other routes and return index file
app.all('*', (req, res) => {
  res.sendFile(path.join(__dirname, 'pub/index.html'));
});

// create HTTP server
const server = http.createServer(app);

// listen on provided port, on all network interfaces
server.listen(port, () => console.log(`API running on ${process.env.SERVICE_PATH}`));
