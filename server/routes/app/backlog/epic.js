const epic = {
  getAll: (req, res) => {
    const tds = require('../../tds-connection');
    const qryStr = `
      select e.Id,
        e.Title,
        e.Description,
        e.Priority
      from AgileEpic e
        inner join AgileProduct p on e.ProductId = p.Id
      where p.Id = @prodid
        and p.AuthGroupId = @agid
      order by e.Priority
    `;
    const params = [
      { name: 'agid', type: 'NVarChar', value: req.app.get('agid'), length: 35 },
      { name: 'prodid', type: 'NVarChar', value: req.params.prodid, length: 35 }
    ];
    
    tds.select(req, res, qryStr, params);
  },

  getById: (req, res) => {
    const tds = require('../../tds-connection');
    const qryStr = `
      select Id,
        ProductId,
        Title,
        Description,
        Priority,
        DateCreated,
        OwnerId,
        LastModified
      from AgileEpic
      where Id = @id
        and ProductId = @prodid
    `;
    const params = [
      { name: 'prodid', type: 'NVarChar', value: req.params.prodid, length: 35 },
      { name: 'id', type: 'NVarChar', value: req.params.id, length: 35 }
    ];

    tds.selectOne(req, res, qryStr, params, 'epic');
  },

  getExportData: (req, res, next) => {
    const tds = require('../../tds-connection');
    const qryStr = `
      select e.Title,
        e.Description,
        e.Priority
      from AgileEpic e
        inner join AgileProduct p on e.ProductId = p.Id
      where p.Id = @prodid
        and p.AuthGroupId = @agid
      order by e.Priority
    `;
    const params = [
      { name: 'agid', type: 'NVarChar', value: req.app.get('agid'), length: 35 },
      { name: 'prodid', type: 'NVarChar', value: req.params.prodid, length: 35 }
    ];
    
    tds.select(req, res, qryStr, params, 'epics', next);
  },

  getExport: (req, res) => {
    const excel = require('node-excel-export');
    const styles = { hdr: { font: { bold: true } } };
    const spec = {
      Title: {
        displayName: 'Name',
        headerStyle: styles.hdr,
        width: '20'
      },
      Description: {
        displayName: 'Description',
        headerStyle: styles.hdr,
        width: '50'
      },
      Priority: {
        displayName: 'Priority',
        headerStyle: styles.hdr,
        width: '10'
      }
    };
    const data = res.locals._epics;
    const rpt = excel.buildExport(
      [
        {
          name: 'Epics',
          specification: spec,
          data: data
        }
      ]
    );

    res.attachment('epics.xlsx');

    return res.status(200).send(rpt);
  },

  getList: (req, res) => {
    const tds = require('../../tds-connection');
    const qryStr = `
      select e.Id,
        e.Title,
        e.Description,
        e.Priority
      from AgileEpic e
        inner join AgileProduct p on e.ProductId = p.Id
      where p.Id = @prodid
        and p.AuthGroupId = @agid
      order by e.Title
    `;
    const params = [
      { name: 'agid', type: 'NVarChar', value: req.app.get('agid'), length: 35 },
      { name: 'prodid', type: 'NVarChar', value: req.params.prodid, length: 35 }
    ];
    
    tds.select(req, res, qryStr, params);
  },

  delete: (req, res) => {
    const tds = require('../../tds-connection');
    const qryStr = `
      delete from AgileEpic
      where Id = @id
        and ProductId = @prodid
    `;
    const params = [
      { name: 'id', type: 'NVarChar', value: req.params.id, length: 35 },
      { name: 'prodid', type: 'NVarChar', value: req.params.prodid, length: 35 }
    ];
    
    tds.delete(req, res, qryStr, params);
  },

  insert: (req, res) => {
    const tds = require('../../tds-connection');
    const qryStr = `
      declare @newpri int

      if (@pri is null)
        begin
          select @newpri = isnull(max(Priority), 0) + 1
          from AgileEpic
          where ProductId = @prodid
        end
      else
        set @newpri = @pri

      insert into AgileEpic (
        Id,
        ProductId,
        Title,
        Description,
        Priority,
        DateCreated,
        OwnerId,
        LastModified
      ) values (
        @id,
        @prodid,
        @ttl,
        @desc,
        @newpri,
        @dt,
        @usr,
        @dt
      )
    `;
    const params = [
      { name: 'id', type: 'NVarChar', value: req.body.Id, length: 35 },
      { name: 'prodid', type: 'NVarChar', value: req.params.prodid, length: 35 },
      { name: 'ttl', type: 'NVarChar', value: req.body.Title, length: 50 },
      { name: 'desc', type: 'NVarChar', value: req.body.Description, length: 4001 },
      { name: 'pri', type: 'Int', value: req.body.Priority || null },
      { name: 'dt', type: 'DateTime', value: new Date() },
      { name: 'usr', type: 'NVarChar', value: res.locals._current_user_id, length: 35 }
    ];
    
    tds.insert(req, res, qryStr, params);
  },

  update: (req, res) => {
    const tds = require('../../tds-connection');
    const qryStr = `
      declare @newpri int

      if (@pri is null)
        begin
          select @newpri = isnull(max(Priority), 0) + 1
          from AgileEpic
          where ProductId = @prodid
        end
      else
        set @newpri = @pri

      update AgileEpic
      set Title = @ttl,
        Description = @desc,
        Priority = @newpri,
        OwnerId = @usr,
        LastModified = @dt
      where Id = @id
        and ProductId = @prodid
    `;
    const params = [
      { name: 'id', type: 'NVarChar', value: req.params.id, length: 35 },
      { name: 'prodid', type: 'NVarChar', value: req.params.prodid, length: 35 },
      { name: 'ttl', type: 'NVarChar', value: req.body.Title, length: 50 },
      { name: 'desc', type: 'NVarChar', value: req.body.Description, length: 4001 },
      { name: 'pri', type: 'Int', value: req.body.Priority || null },
      { name: 'usr', type: 'NVarChar', value: res.locals._current_user_id, length: 35 },
      { name: 'dt', type: 'DateTime', value: new Date() }
    ];
    
    tds.update(req, res, qryStr, params);
  }
};

module.exports = epic;
