const express = require('express');
const router = express.Router();

const feature = require('./feature');

router.get(
  '/:prodid/excel',
  (req, res, next) => feature.getExportData(req, res, next),
  (req, res) => feature.getExport(req, res)
);

router.get(
  '/:prodid/excelbyepic',
  (req, res, next) => feature.getExportDataByEpic(req, res, next),
  (req, res) => feature.getExportByEpic(req, res)
);

router.get('/:prodid/:epicid/list', (req, res) => feature.getList(req, res));

router.get('/:prodid/:epicid/:featid', (req, res) => feature.getById(req, res));

router.get('/:prodid/:epicid', (req, res) => feature.getAll(req, res));

module.exports = router;
