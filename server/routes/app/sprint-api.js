const express = require('express');
const router = express.Router();

const sprint = require('./sprint');

router.get('/:prodid/release/:relid', (req, res) => sprint.getByRelease(req, res));

router.get('/:prodid/free', (req, res) => sprint.getFree(req, res));

router.get('/:prodid/:id', (req, res) => sprint.getById(req, res));

router.get('/:prodid', (req, res) => sprint.getAll(req, res));

module.exports = router;
