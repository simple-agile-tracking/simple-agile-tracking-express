const story = {
  getAll: (req, res) => {
    const tds = require('../../tds-connection');
    const qryStr = `
      select st.Id,
        st.ProductId,
        st.Priority,
        st.Name,
        st.Description,
        st.Acceptance,
        st.RisksDependencies,
        st.StorySize,
        st.FeatureId,
        f.Title as FeatureTitle,
        f.Priority as FeaturePriority,
        f.EpicId,
        e.Title as EpicTitle,
        e.Priority as EpicPriority,
        st.SprintId,
        s.SprintNumber,
        s.StartDate,
        s.EndDate,
        s.ReleaseId,
        r.ReleaseNumber,
        r.ReleaseDate,
        st.IsComplete,
        st.QANotes
      from AgileProduct p
        inner join AgileStory st on p.Id = st.ProductId
        left outer join AgileSprint s on st.SprintId = s.Id
        left outer join AgileRelease r on s.ReleaseId = r.Id
        left outer join (
          AgileFeature f
          inner join AgileEpic e on f.EpicId = e.Id
        ) on st.FeatureId = f.Id
          and p.Id = e.ProductId
      where p.Id = @prodid
        and p.AuthGroupId = @agid
      order by case when st.Priority is null then 1 else 0 end, st.Priority, st.Name
    `;
    const params = [
      { name: 'agid', type: 'NVarChar', value: req.app.get('agid'), length: 35 },
      { name: 'prodid', type: 'NVarChar', value: req.params.prodid, length: 35 }
    ];
    
    tds.select(req, res, qryStr, params);
  },

  getByFeature: (req, res) => {
    const tds = require('../../tds-connection');
    const qryStr = `
      select st.Id,
        st.ProductId,
        st.Priority,
        st.Name,
        st.Description,
        st.Acceptance,
        st.RisksDependencies,
        st.StorySize,
        st.FeatureId,
        st.SprintId,
        s.SprintNumber,
        s.StartDate,
        s.EndDate,
        s.ReleaseId,
        r.ReleaseNumber,
        r.ReleaseDate,
        st.IsComplete,
        st.QANotes
      from AgileProduct p
        inner join AgileEpic e on p.Id = e.ProductId
        inner join AgileFeature f on e.Id = f.EpicId
        inner join AgileStory st on f.Id = st.FeatureId
        left outer join AgileSprint s on st.SprintId = s.Id
        left outer join AgileRelease r on s.ReleaseId = r.Id
      where f.Id = @featureid
        and e.Id = @epicid
        and p.Id = @prodid
        and p.AuthGroupId = @agid
      order by case when st.Priority is null then 1 else 0 end, st.Priority, st.Name
    `;
    const params = [
      { name: 'agid', type: 'NVarChar', value: req.app.get('agid'), length: 35 },
      { name: 'featureid', type: 'NVarChar', value: req.params.featid, length: 35 },
      { name: 'epicid', type: 'NVarChar', value: req.params.epicid, length: 35 },
      { name: 'prodid', type: 'NVarChar', value: req.params.prodid, length: 35 }
    ];
    
    tds.select(req, res, qryStr, params);
  },

  getById: (req, res, next) => {
    const tds = require('../../tds-connection');
    const qryStr = `
      select st.Id,
        st.ProductId,
        st.Priority,
        st.Name,
        st.Description,
        st.Acceptance,
        st.RisksDependencies,
        st.StorySize,
        st.FeatureId,
        st.SprintId,
        s.SprintNumber,
        s.StartDate,
        s.EndDate,
        s.ReleaseId,
        r.ReleaseNumber,
        r.ReleaseDate,
        st.IsComplete,
        st.QANotes,
        st.DateCreated,
        st.OwnerId,
        st.LastModified
      from AgileProduct p
        inner join AgileStory st on p.Id = st.ProductId
        left outer join AgileSprint s on st.SprintId = s.Id
        left outer join AgileRelease r on s.ReleaseId = r.Id
      where st.Id = @itemid
        and p.Id = @prodid
        and p.AuthGroupId = @agid
    `;
    const params = [
      { name: 'agid', type: 'NVarChar', value: req.app.get('agid'), length: 35 },
      { name: 'itemid', type: 'NVarChar', value: req.params.itemid, length: 35 },
      { name: 'prodid', type: 'NVarChar', value: req.params.prodid, length: 35 }
    ];

    tds.selectOne(req, res, qryStr, params, 'story', next);
  },

  getBySprint: (req, res) => {
    const tds = require('../../tds-connection');
    const qryStr = `
      select st.Id,
        st.ProductId,
        st.Priority,
        st.Name,
        st.Description,
        st.Acceptance,
        st.RisksDependencies,
        st.StorySize,
        st.FeatureId,
        st.SprintId,
        s.SprintNumber,
        s.StartDate,
        s.EndDate,
        s.ReleaseId,
        r.ReleaseNumber,
        r.ReleaseDate,
        st.IsComplete,
        st.QANotes
      from AgileProduct p
        inner join AgileStory st on p.Id = st.ProductId
        inner join AgileSprint s on st.SprintId = s.Id
        left outer join AgileRelease r on s.ReleaseId = r.Id
      where s.Id = @sprintid
        and p.Id = @prodid
        and p.AuthGroupId = @agid
      order by st.Name
    `;
    const params = [
      { name: 'agid', type: 'NVarChar', value: req.app.get('agid'), length: 35 },
      { name: 'sprintid', type: 'NVarChar', value: req.params.sprintid, length: 35 },
      { name: 'prodid', type: 'NVarChar', value: req.params.prodid, length: 35 }
    ];
    
    tds.select(req, res, qryStr, params);
  },

  getExportData: (req, res, next) => {
    const tds = require('../../tds-connection');
    const qryStr = `
      select st.Priority,
        st.Name,
        st.Description,
        st.StorySize,
        case
          when st.IsComplete = 1 then 'YES'
          else 'No'
        end as Completed,
        f.Title as FeatureTitle,
        f.Priority as FeaturePriority,
        s.SprintNumber,
        s.EndDate,
        r.ReleaseNumber,
        r.ReleaseDate
      from AgileProduct p
        inner join AgileStory st on p.Id = st.ProductId
        left outer join AgileSprint s on st.SprintId = s.Id
        left outer join AgileRelease r on s.ReleaseId = r.Id
        left outer join (
          AgileFeature f
          inner join AgileEpic e on f.EpicId = e.Id
        ) on st.FeatureId = f.Id
          and p.Id = e.ProductId
      where p.Id = @prodid
        and p.AuthGroupId = @agid
      order by case when st.Priority is null then 1 else 0 end, st.Priority, st.Name
    `;
    const params = [
      { name: 'agid', type: 'NVarChar', value: req.app.get('agid'), length: 35 },
      { name: 'prodid', type: 'NVarChar', value: req.params.prodid, length: 35 }
    ];
    
    tds.select(req, res, qryStr, params, 'backlog', next);
  },

  getExport: (req, res) => {
    const excel = require('node-excel-export');
    const styles = { hdr: { font: { bold: true } } };
    const spec = {
      Name: {
        displayName: 'Item',
        headerStyle: styles.hdr,
        width: '20'
      },
      Description: {
        displayName: 'Description',
        headerStyle: styles.hdr,
        width: '50'
      },
      Priority: {
        displayName: 'Priority',
        headerStyle: styles.hdr,
        width: '10'
      },
      StorySize: {
        displayName: 'Size',
        headerStyle: styles.hdr,
        width: '10'
      },
      Completed: {
        displayName: 'Complete',
        headerStyle: styles.hdr,
        width: '10'
      },
      FeatureTitle: {
        displayName: 'Feature',
        headerStyle: styles.hdr,
        width: '20'
      },
      FeaturePriority: {
        displayName: 'Feature Priority',
        headerStyle: styles.hdr,
        width: '10'
      },
      SprintNumber: {
        displayName: 'Sprint',
        headerStyle: styles.hdr,
        width: '10'
      },
      EndDate: {
        displayName: 'Sprint End',
        headerStyle: styles.hdr,
        width: '15'
      },
      ReleaseNumber: {
        displayName: 'Release',
        headerStyle: styles.hdr,
        width: '10'
      },
      ReleaseDate: {
        displayName: 'Release Date',
        headerStyle: styles.hdr,
        width: '15'
      }
    };
    const data = res.locals._backlog;
    const rpt = excel.buildExport(
      [
        {
          name: 'Ordered Backlog',
          specification: spec,
          data: data
        }
      ]
    );

    res.attachment('backlog.xlsx');

    return res.status(200).send(rpt);
  },

  getExportDataByEpic: (req, res, next) => {
    const tds = require('../../tds-connection');
    const qryStr = `
      select e.Title as EpicTitle,
        e.Priority as EpicPriority,
        f.Title as FeatureTitle,
        f.Priority as FeaturePriority,
        st.Priority,
        st.Name,
        st.StorySize,
        case
          when st.IsComplete = 1 then 'YES'
          else 'No'
        end as Completed,
        s.SprintNumber,
        s.EndDate,
        r.ReleaseNumber,
        r.ReleaseDate
      from AgileProduct p
        inner join AgileEpic e on p.Id = e.ProductId
        inner join AgileFeature f on e.Id = f.EpicId
        inner join AgileStory st on f.Id = st.FeatureId
        left outer join AgileSprint s on st.SprintId = s.Id
        left outer join AgileRelease r on s.ReleaseId = r.Id
      where p.Id = @prodid
        and p.AuthGroupId = @agid
      order by e.Priority,
        case when f.Priority is null then 1 else 0 end,
        f.Priority,
        case when st.Priority is null then 1 else 0 end,
        st.Priority,
        st.Name
    `;
    const params = [
      { name: 'agid', type: 'NVarChar', value: req.app.get('agid'), length: 35 },
      { name: 'prodid', type: 'NVarChar', value: req.params.prodid, length: 35 }
    ];
    
    tds.select(req, res, qryStr, params, 'stories', next);
  },

  getExportByEpic: (req, res) => {
    const excel = require('node-excel-export');
    const styles = { hdr: { font: { bold: true } } };
    const spec = {
      EpicTitle: {
        displayName: 'Epic',
        headerStyle: styles.hdr,
        width: '20'
      },
      EpicPriority: {
        displayName: 'Epic Priority',
        headerStyle: styles.hdr,
        width: '10'
      },
      FeatureTitle: {
        displayName: 'Feature',
        headerStyle: styles.hdr,
        width: '20'
      },
      FeaturePriority: {
        displayName: 'Feature Priority',
        headerStyle: styles.hdr,
        width: '10'
      },
      Name: {
        displayName: 'Item',
        headerStyle: styles.hdr,
        width: '20'
      },
      Description: {
        displayName: 'Description',
        headerStyle: styles.hdr,
        width: '50'
      },
      Priority: {
        displayName: 'Priority',
        headerStyle: styles.hdr,
        width: '10'
      },
      StorySize: {
        displayName: 'Size',
        headerStyle: styles.hdr,
        width: '10'
      },
      Completed: {
        displayName: 'Complete',
        headerStyle: styles.hdr,
        width: '10'
      },
      SprintNumber: {
        displayName: 'Sprint',
        headerStyle: styles.hdr,
        width: '10'
      },
      EndDate: {
        displayName: 'Sprint End',
        headerStyle: styles.hdr,
        width: '15'
      },
      ReleaseNumber: {
        displayName: 'Release',
        headerStyle: styles.hdr,
        width: '10'
      },
      ReleaseDate: {
        displayName: 'Release Date',
        headerStyle: styles.hdr,
        width: '15'
      }
    };
    const data = res.locals._stories;
    const rpt = excel.buildExport(
      [
        {
          name: 'Backlog by Epic',
          specification: spec,
          data: data
        }
      ]
    );

    res.attachment('backlogByEpic.xlsx');

    return res.status(200).send(rpt);
  },

  getFree: (req, res) => {
    const tds = require('../../tds-connection');
    const qryStr = `
      select st.Id,
        st.ProductId,
        st.Priority,
        st.Name,
        st.Description,
        st.StorySize,
        st.SprintId
      from AgileProduct p
        inner join AgileStory st on p.Id = st.ProductId
      where p.Id = @prodid
        and p.AuthGroupId = @agid
        and (
          st.SprintId is null
          or st.SprintId = ''
        )
      order by case when st.Priority is null then 1 else 0 end, st.Priority, st.Name
    `;
    const params = [
      { name: 'agid', type: 'NVarChar', value: req.app.get('agid'), length: 35 },
      { name: 'prodid', type: 'NVarChar', value: req.params.prodid, length: 35 }
    ];
    
    tds.select(req, res, qryStr, params);
  },

  convert: (req, res) => {
    const tds = require('../../tds-connection');
    const qryStr = `
      insert into AgileStory (
        Id,
        ProductId,
        Priority,
        Name,
        Description,
        Acceptance,
        RisksDependencies,
        StorySize,
        FeatureId,
        SprintId,
        IsComplete,
        QANotes,
        DateCreated,
        OwnerId,
        LastModified
      ) values (
        @id,
        @prodid,
        @pri,
        @nm,
        @desc,
        '',
        @rds,
        null,
        @featid,
        @sprint,
        0,
        '',
        @dt,
        @usr,
        @dt
      );

      delete from AgileFeature
      where Id = @id;
    `;
    const params = [
      { name: 'id', type: 'NVarChar', value: req.params.id, length: 35 },
      { name: 'prodid', type: 'NVarChar', value: req.params.prodid, length: 35 },
      { name: 'pri', type: 'Int', value: req.body.Priority },
      { name: 'nm', type: 'NVarChar', value: req.body.Name, length:50 },
      { name: 'desc', type: 'NVarChar', value: req.body.Description, length: 4001 },
      { name: 'rds', type: 'NVarChar', value: req.body.RisksDependencies, length: 4001 },
      { name: 'featid', type: 'NVarChar', value: req.body.FeatureId, length: 35 },
      { name: 'sprint', type: 'NVarChar', value: req.body.SprintId, length: 35 },
      { name: 'dt', type: 'DateTime', value: new Date() },
      { name: 'usr', type: 'NVarChar', value: res.locals._current_user_id, length: 35 }
    ];
    
    tds.insert(req, res, qryStr, params);
  },

  delete: (req, res) => {
    const tds = require('../../tds-connection');
    const qryStr = `
      delete from AgileStory
      where Id = @id
        and ProductId = @prodid
    `;
    const params = [
      { name: 'id', type: 'NVarChar', value: req.params.id, length: 35 },
      { name: 'prodid', type: 'NVarChar', value: req.params.prodid, length: 35 }
    ];
    
    tds.delete(req, res, qryStr, params);
  },

  insert: (req, res) => {
    const tds = require('../../tds-connection');
    const qryStr = `
      insert into AgileStory (
        Id,
        ProductId,
        Priority,
        Name,
        Description,
        Acceptance,
        RisksDependencies,
        StorySize,
        FeatureId,
        SprintId,
        IsComplete,
        QANotes,
        DateCreated,
        OwnerId,
        LastModified
      ) values (
        @id,
        @prodid,
        @pri,
        @nm,
        @desc,
        @acc,
        @rds,
        @sz,
        @featid,
        @sprint,
        @done,
        @qa,
        @dt,
        @usr,
        @dt
      )
    `;
    const params = [
      { name: 'id', type: 'NVarChar', value: req.body.Id, length: 35 },
      { name: 'prodid', type: 'NVarChar', value: req.params.prodid, length: 35 },
      { name: 'pri', type: 'Int', value: req.body.Priority },
      { name: 'nm', type: 'NVarChar', value: req.body.Name, length: 50 },
      { name: 'desc', type: 'NVarChar', value: req.body.Description, length: 4001 },
      { name: 'acc', type: 'NVarChar', value: req.body.Acceptance, length: 4001 },
      { name: 'rds', type: 'NVarChar', value: req.body.RisksDependencies, length: 4001 },
      { name: 'sz', type: 'Int', value: req.body.StorySize || null },
      { name: 'featid', type: 'NVarChar', value: req.body.FeatureId, length: 35 },
      { name: 'sprint', type: 'NVarChar', value: req.body.SprintId, length: 35 },
      { name: 'done', type: 'Bit', value: req.body.IsComplete },
      { name: 'qa', type: 'NVarChar', value: req.body.QANotes, length: 4001 },
      { name: 'dt', type: 'DateTime', value: new Date() },
      { name: 'usr', type: 'NVarChar', value: res.locals._current_user_id, length: 35 }
    ];
    
    tds.insert(req, res, qryStr, params);
  },

  update: (req, res) => {
    const tds = require('../../tds-connection');
    const qryStr = `
      update AgileStory
      set Priority = @pri,
        Name = @nm,
        Description = @desc,
        Acceptance = @acc,
        RisksDependencies = @rds,
        StorySize = @sz,
        FeatureId = @featid,
        SprintId = @sprint,
        IsComplete = @done,
        QANotes = @qa,
        OwnerId = @usr,
        LastModified = @dt
      where Id = @id
        and ProductId = @prodid
    `;
    const params = [
      { name: 'id', type: 'NVarChar', value: req.params.id, length: 35 },
      { name: 'prodid', type: 'NVarChar', value: req.params.prodid, length: 35 },
      { name: 'pri', type: 'Int', value: req.body.Priority },
      { name: 'nm', type: 'NVarChar', value: req.body.Name, length: 50 },
      { name: 'desc', type: 'NVarChar', value: req.body.Description, length: 4001 },
      { name: 'acc', type: 'NVarChar', value: req.body.Acceptance, length: 4001 },
      { name: 'rds', type: 'NVarChar', value: req.body.RisksDependencies, length: 4001 },
      { name: 'sz', type: 'Int', value: req.body.StorySize || null },
      { name: 'featid', type: 'NVarChar', value: req.body.FeatureId, length: 35 },
      { name: 'sprint', type: 'NVarChar', value: req.body.SprintId, length: 35 },
      { name: 'done', type: 'Bit', value: req.body.IsComplete },
      { name: 'qa', type: 'NVarChar', value: req.body.QANotes, length: 4001 },
      { name: 'usr', type: 'NVarChar', value: res.locals._current_user_id, length: 35 },
      { name: 'dt', type: 'DateTime', value: new Date() }
    ];
    
    tds.update(req, res, qryStr, params);
  },

  updateBySprint: (req, res) => {
    const tds = require('../../tds-connection');
    const qryStr = `
      update AgileStory
      set Priority = @pri,
        Name = @nm,
        Description = @desc,
        Acceptance = @acc,
        RisksDependencies = @rds,
        StorySize = @sz,
        IsComplete = @done,
        QANotes = @qa,
        OwnerId = @usr,
        LastModified = @dt
      where Id = @id
        and SprintId = @sprintid
        and ProductId = @prodid
    `;
    const params = [
      { name: 'id', type: 'NVarChar', value: req.params.id, length: 35 },
      { name: 'prodid', type: 'NVarChar', value: req.params.prodid, length: 35 },
      { name: 'sprintid', type: 'NVarChar', value: req.params.sprintid, length: 35 },
      { name: 'pri', type: 'Int', value: req.body.Priority },
      { name: 'nm', type: 'NVarChar', value: req.body.Name, length: 50 },
      { name: 'desc', type: 'NVarChar', value: req.body.Description, length: 4001 },
      { name: 'acc', type: 'NVarChar', value: req.body.Acceptance, length: 4001 },
      { name: 'rds', type: 'NVarChar', value: req.body.RisksDependencies, length: 4001 },
      { name: 'sz', type: 'Int', value: req.body.StorySize || null },
      { name: 'done', type: 'Bit', value: req.body.IsComplete || false },
      { name: 'qa', type: 'NVarChar', value: req.body.QANotes, length: 4001 },
      { name: 'usr', type: 'NVarChar', value: res.locals._current_user_id, length: 35 },
      { name: 'dt', type: 'DateTime', value: new Date() }
    ];
    
    tds.update(req, res, qryStr, params);
  }
};

module.exports = story;
