const sprint = {
  getAll: (req, res) => {
    const tds = require('../tds-connection');
    const qryStr = `
      select s.Id,
        s.SprintNumber,
        s.Goal,
        s.StartDate,
        s.EndDate,
        s.ReleaseId,
        r.ReleaseNumber,
        r.ReleaseDate
      from AgileProduct p
        inner join AgileSprint s on p.Id = s.ProductId
        left outer join AgileRelease r on s.ReleaseId = r.Id
      where p.Id = @prodid
        and p.AuthGroupId = @agid
      order by s.SprintNumber desc
    `;
    const params = [
      { name: 'agid', type: 'NVarChar', value: req.app.get('agid'), length: 35 },
      { name: 'prodid', type: 'NVarChar', value: req.params.prodid, length: 35 }
    ];
    
    tds.select(req, res, qryStr, params);
  },

  getById: (req, res) => {
    const tds = require('../tds-connection');
    const qryStr = `
      select s.Id,
        s.ProductId,
        s.SprintNumber,
        s.Goal,
        s.StartDate,
        s.EndDate,
        s.ReleaseId,
        r.ReleaseNumber,
        r.ReleaseDate,
        s.DateCreated,
        s.OwnerId,
        s.LastModified
      from AgileProduct p
        inner join AgileSprint s on p.Id = s.ProductId
        left outer join AgileRelease r on s.ReleaseId = r.Id
      where s.Id = @id
        and p.Id = @prodid
        and p.AuthGroupId = @agid
    `;
    const params = [
      { name: 'agid', type: 'NVarChar', value: req.app.get('agid'), length: 35 },
      { name: 'prodid', type: 'NVarChar', value: req.params.prodid, length: 35 },
      { name: 'id', type: 'NVarChar', value: req.params.id, length: 35 }
    ];
    
    tds.selectOne(req, res, qryStr, params, 'sprint');
  },

  getByRelease: (req, res) => {
    const tds = require('../tds-connection');
    const qryStr = `
      select s.Id,
        s.SprintNumber,
        s.Goal,
        s.StartDate,
        s.EndDate
      from AgileProduct p
        inner join AgileSprint s on p.Id = s.ProductId
        inner join AgileRelease r on s.ReleaseId = r.Id
      where r.Id = @relid
        and p.Id = @prodid
        and p.AuthGroupId = @agid
      order by s.SprintNumber desc
    `;
    const params = [
      { name: 'agid', type: 'NVarChar', value: req.app.get('agid'), length: 35 },
      { name: 'prodid', type: 'NVarChar', value: req.params.prodid, length: 35 },
      { name: 'relid', type: 'NVarChar', value: req.params.relid, length: 35 }
    ];
    
    tds.select(req, res, qryStr, params);
  },

  getFree: (req, res) => {
    const tds = require('../tds-connection');
    const qryStr = `
      select s.Id,
        s.SprintNumber,
        s.Goal,
        s.StartDate,
        s.EndDate,
        s.ReleaseId
      from AgileProduct p
        inner joing AgileSprint s on p.Id = s.ProductId
      where p.Id = @prodid
        and p.AuthGroupId = @agid
        and (
          s.ReleaseId is null
          or s.ReleaseId = ''
        )
      order by s.SprintNumber
    `;
    const params = [
      { name: 'agid', type: 'NVarChar', value: req.app.get('agid'), length: 35 },
      { name: 'prodid', type: 'NVarChar', value: req.params.prodid, length: 35 }
    ];
    
    tds.select(req, res, qryStr, params);
  },

  addItem: (req, res) => {
    const tds = require('../tds-connection');
    const qryStr = `
      update AgileStory
      set SprintId = @id,
        OwnerId = @usr,
        LastModified = @dt
      where Id = @sty
        and (
          SprintId is null
          or SprintId = ''
        )
    `;
    const params = [
      { name: 'agid', type: 'NVarChar', value: req.app.get('agid'), length: 35 },
      { name: 'id', type: 'NVarChar', value: req.params.id, length: 35 },
      { name: 'sty', type: 'NVarChar', value: req.body.StoryId, length: 35 },
      { name: 'usr', type: 'NVarChar', value: res.locals._current_user_id, length: 35 },
      { name: 'dt', type: 'DateTime', value: new Date() }
    ];
    
    tds.update(req, res, qryStr, params);
  },

  delete: (req, res) => {
    const tds = require('../tds-connection');
    const qryStr = `
      delete from AgileSprint
      where Id = @id
        and ProductId = @prodid
    `;
    const params = [
      { name: 'id', type: 'NVarChar', value: req.params.id, length: 35 },
      { name: 'prodid', type: 'NVarChar', value: req.params.prodid, length: 35 }
    ];
    
    tds.delete(req, res, qryStr, params);
  },

  insert: (req, res) => {
    const tds = require('../tds-connection');
    const qryStr = `
      insert into AgileSprint (
        Id,
        ProductId,
        SprintNumber,
        Goal,
        StartDate,
        EndDate,
        ReleaseId,
        DateCreated,
        OwnerId,
        LastModified
      ) values (
        @id,
        @prodid,
        @cnt,
        @goal,
        @start,
        @end,
        @rel,
        @dt,
        @usr,
        @dt
      )
    `;
    const params = [
      { name: 'id', type: 'NVarChar', value: req.body.Id, length: 35 },
      { name: 'prodid', type: 'NVarChar', value: req.params.prodid, length: 35 },
      { name: 'cnt', type: 'Int', value: req.body.SprintNumber },
      { name: 'goal', type: 'NVarChar', value: req.body.Goal, length: 4001 },
      { name: 'start', type: 'Date', value: new Date(req.body.StartDate) },
      { name: 'end', type: 'Date', value: new Date(req.body.EndDate) },
      { name: 'rel', type: 'NVarChar', value: req.body.ReleaseId, length: 35 },
      { name: 'dt', type: 'DateTime', value: new Date() },
      { name: 'usr', type: 'NVarChar', value: res.locals._current_user_id, length: 35 }
    ];
    
    tds.insert(req, res, qryStr, params);
  },

  removeItem: (req, res) => {
    const tds = require('../tds-connection');
    const qryStr = `
      update AgileStory
      set SprintId = null,
        OwnerId = @usr,
        LastModified = @dt
      where Id = @sty
        and SprintId = @id
    `;
    const params = [
      { name: 'id', type: 'NVarChar', value: req.params.id, length: 35 },
      { name: 'sty', type: 'NVarChar', value: req.params.storyid, length: 35 },
      { name: 'usr', type: 'NVarChar', value: res.locals._current_user_id, length: 35 },
      { name: 'dt', type: 'DateTime', value: new Date() }
    ];
    
    tds.delete(req, res, qryStr, params);
  },

  update: (req, res) => {
    const tds = require('../tds-connection');
    const qryStr = `
      update AgileSprint
      set SprintNumber = @cnt,
        Goal = @goal,
        StartDate = @start,
        EndDate = @end,
        ReleaseId = @rel,
        OwnerId = @usr,
        LastModified = @dt
      where Id = @id
        and ProductId = @prodid
    `;
    const params = [
      { name: 'id', type: 'NVarChar', value: req.params.id, length: 35 },
      { name: 'prodid', type: 'NVarChar', value: req.params.prodid, length: 35 },
      { name: 'cnt', type: 'Int', value: req.body.SprintNumber },
      { name: 'goal', type: 'NVarChar', value: req.body.Goal, length: 4001 },
      { name: 'start', type: 'Date', value: new Date(req.body.StartDate) },
      { name: 'end', type: 'Date', value: new Date(req.body.EndDate) },
      { name: 'rel', type: 'NVarChar', value: req.body.ReleaseId, length: 35 },
      { name: 'usr', type: 'NVarChar', value: res.locals._current_user_id, length: 35 },
      { name: 'dt', type: 'DateTime', value: new Date() }
    ];
    
    tds.update(req, res, qryStr, params);
  }
};

module.exports = sprint;
