const express = require('express');
const router = express.Router();

const feature = require('./feature');

router.delete('/:prodid/:epicid/:id', (req, res) => feature.delete(req, res));

router.post('/:prodid/:epicid/convert/:id', (req, res) => feature.convert(req, res));

router.post('/:prodid/:epicid', (req, res) => feature.insert(req, res));

router.put('/:prodid/:epicid/:id', (req, res) => feature.update(req, res));

module.exports = router;
