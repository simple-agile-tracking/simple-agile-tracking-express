/* this script will create all the necessary Agile management tables in a SQL Server database */

/****** Object:  Table [dbo].[AgileEpic]    Script Date: 7/12/2019 10:53:36 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[AgileEpic](
	[Id] [nvarchar](35) NOT NULL,
	[ProductId] [nvarchar](35) NOT NULL,
	[Title] [nvarchar](50) NOT NULL,
	[Description] [nvarchar](max) NULL,
	[Priority] [int] NOT NULL,
	[DateCreated] [datetime] NOT NULL,
	[OwnerId] [nvarchar](35) NOT NULL,
	[LastModified] [datetime] NOT NULL,
 CONSTRAINT [PK_AgileEpic] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

/****** Object:  Table [dbo].[AgileFeature]    Script Date: 7/12/2019 10:53:36 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[AgileFeature](
	[Id] [nvarchar](35) NOT NULL,
	[EpicId] [nvarchar](35) NOT NULL,
	[Title] [nvarchar](50) NOT NULL,
	[Description] [nvarchar](max) NULL,
	[RisksDependencies] [nvarchar](max) NULL,
	[Priority] [int] NULL,
	[DateCreated] [datetime] NOT NULL,
	[OwnerId] [nvarchar](35) NOT NULL,
	[LastModified] [datetime] NOT NULL,
 CONSTRAINT [PK_AgileFeature] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

/****** Object:  Table [dbo].[AgileIssue]    Script Date: 7/12/2019 10:53:36 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[AgileIssue](
	[Id] [nvarchar](35) NOT NULL,
	[StoryId] [nvarchar](35) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Description] [nvarchar](max) NOT NULL,
	[AssignedTo] [nvarchar](35) NULL,
	[Status] [nvarchar](50) NOT NULL,
	[DateCreated] [datetime] NOT NULL,
	[OwnerId] [nvarchar](35) NOT NULL,
	[LastModified] [datetime] NOT NULL,
 CONSTRAINT [PK_AgileIssue] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

/****** Object:  Table [dbo].[AgileProduct]    Script Date: 7/12/2019 10:53:36 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[AgileProduct](
	[Id] [nvarchar](35) NOT NULL,
	[AuthGroupId] [nvarchar](35) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Description] [nvarchar](max) NULL,
	[SprintLength] [smallint] NULL,
	[InBacklogPhase] [bit] NOT NULL,
	[DateCreated] [datetime] NOT NULL,
	[OwnerId] [nvarchar](35) NOT NULL,
	[LastModified] [datetime] NOT NULL,
 CONSTRAINT [PK_AgileProduct] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

/****** Object:  Table [dbo].[AgileRelease]    Script Date: 7/12/2019 10:53:36 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[AgileRelease](
	[Id] [nvarchar](35) NOT NULL,
	[ProductId] [nvarchar](35) NOT NULL,
	[ReleaseNumber] [int] NOT NULL,
	[ReleaseDate] [datetime] NULL,
	[Notes] [nvarchar](max) NULL,
	[DateCreated] [datetime] NOT NULL,
	[OwnerId] [nvarchar](35) NOT NULL,
	[LastModified] [datetime] NOT NULL,
 CONSTRAINT [PK_AgileRelease] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

/****** Object:  Table [dbo].[AgileSprint]    Script Date: 7/12/2019 10:53:37 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[AgileSprint](
	[Id] [nvarchar](35) NOT NULL,
	[ProductId] [nvarchar](35) NOT NULL,
	[SprintNumber] [int] NOT NULL,
	[Goal] [nvarchar](max) NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[ReleaseId] [nvarchar](35) NULL,
	[DateCreated] [datetime] NOT NULL,
	[OwnerId] [nvarchar](35) NOT NULL,
	[LastModified] [datetime] NOT NULL,
 CONSTRAINT [PK_AgileSprint] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

/****** Object:  Table [dbo].[AgileStory]    Script Date: 7/12/2019 10:53:37 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[AgileStory](
	[Id] [nvarchar](35) NOT NULL,
	[ProductId] [nvarchar](35) NOT NULL,
	[Priority] [int] NULL,
	[Name] [nvarchar](100) NOT NULL,
	[Description] [nvarchar](max) NULL,
	[Acceptance] [nvarchar](max) NULL,
	[RisksDependencies] [nvarchar](max) NULL,
	[StorySize] [smallint] NULL,
	[FeatureId] [nvarchar](35) NULL,
	[SprintId] [nvarchar](35) NULL,
	[IsComplete] [bit] NOT NULL,
	[QANotes] [nvarchar](max) NULL,
	[DateCreated] [datetime] NOT NULL,
	[OwnerId] [nvarchar](35) NOT NULL,
	[LastModified] [datetime] NOT NULL,
 CONSTRAINT [PK_AgileStory] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

/****** Object:  Table [dbo].[ApplicationLog]    Script Date: 7/12/2019 10:53:37 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ApplicationLog](
	[Id] [nvarchar](35) NOT NULL,
	[ApplicationKey] [nvarchar](35) NOT NULL,
	[UserKey] [nvarchar](128) NOT NULL,
	[UserEmail] [nvarchar](75) NOT NULL,
	[Path] [nvarchar](50) NOT NULL,
	[ActivityDate] [datetime] NOT NULL,
 CONSTRAINT [PK_ApplicationLog] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[AgileProduct] ADD  CONSTRAINT [DF_AgileProduct_InBacklogPhase]  DEFAULT ((1)) FOR [InBacklogPhase]
GO

ALTER TABLE [dbo].[AgileStory] ADD  CONSTRAINT [DF_AgileStory_IsComplete]  DEFAULT ((0)) FOR [IsComplete]
GO

ALTER TABLE [dbo].[AgileEpic]  WITH CHECK ADD  CONSTRAINT [FK_AgileEpic_AgileProduct] FOREIGN KEY([ProductId])
REFERENCES [dbo].[AgileProduct] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[AgileEpic] CHECK CONSTRAINT [FK_AgileEpic_AgileProduct]
GO

ALTER TABLE [dbo].[AgileFeature]  WITH CHECK ADD  CONSTRAINT [FK_AgileFeature_AgileEpic] FOREIGN KEY([EpicId])
REFERENCES [dbo].[AgileEpic] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[AgileFeature] CHECK CONSTRAINT [FK_AgileFeature_AgileEpic]
GO

ALTER TABLE [dbo].[AgileIssue]  WITH CHECK ADD  CONSTRAINT [FK_AgileIssue_AgileStory] FOREIGN KEY([StoryId])
REFERENCES [dbo].[AgileStory] ([Id])
GO

ALTER TABLE [dbo].[AgileIssue] CHECK CONSTRAINT [FK_AgileIssue_AgileStory]
GO

ALTER TABLE [dbo].[AgileProduct]  WITH CHECK ADD  CONSTRAINT [FK_AgileProduct_AuthGroup] FOREIGN KEY([AuthGroupId])
REFERENCES [dbo].[AuthGroup] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[AgileProduct] CHECK CONSTRAINT [FK_AgileProduct_AuthGroup]
GO

ALTER TABLE [dbo].[AgileRelease]  WITH CHECK ADD  CONSTRAINT [FK_AgileRelease_AgileProduct] FOREIGN KEY([ProductId])
REFERENCES [dbo].[AgileProduct] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[AgileRelease] CHECK CONSTRAINT [FK_AgileRelease_AgileProduct]
GO

ALTER TABLE [dbo].[AgileSprint]  WITH CHECK ADD  CONSTRAINT [FK_AgileSprint_AgileProduct] FOREIGN KEY([ProductId])
REFERENCES [dbo].[AgileProduct] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[AgileSprint] CHECK CONSTRAINT [FK_AgileSprint_AgileProduct]
GO

ALTER TABLE [dbo].[AgileSprint]  WITH NOCHECK ADD  CONSTRAINT [FK_AgileSprint_AgileRelease] FOREIGN KEY([ReleaseId])
REFERENCES [dbo].[AgileRelease] ([Id])
NOT FOR REPLICATION 
GO

ALTER TABLE [dbo].[AgileSprint] NOCHECK CONSTRAINT [FK_AgileSprint_AgileRelease]
GO

ALTER TABLE [dbo].[AgileStory]  WITH NOCHECK ADD  CONSTRAINT [FK_AgileStory_AgileFeature] FOREIGN KEY([FeatureId])
REFERENCES [dbo].[AgileFeature] ([Id])
NOT FOR REPLICATION 
GO

ALTER TABLE [dbo].[AgileStory] NOCHECK CONSTRAINT [FK_AgileStory_AgileFeature]
GO

ALTER TABLE [dbo].[AgileStory]  WITH CHECK ADD  CONSTRAINT [FK_AgileStory_AgileProduct] FOREIGN KEY([ProductId])
REFERENCES [dbo].[AgileProduct] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[AgileStory] CHECK CONSTRAINT [FK_AgileStory_AgileProduct]
GO

ALTER TABLE [dbo].[AgileStory]  WITH NOCHECK ADD  CONSTRAINT [FK_AgileStory_AgileSprint] FOREIGN KEY([SprintId])
REFERENCES [dbo].[AgileSprint] ([Id])
NOT FOR REPLICATION 
GO

ALTER TABLE [dbo].[AgileStory] NOCHECK CONSTRAINT [FK_AgileStory_AgileSprint]
GO
