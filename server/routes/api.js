const express = require('express');
const router = express.Router();

const mailer = require('./mailer');

router.get('/', (req, res) => res.status(404).send({ message: 'Invalid API route' }));

router.post('/send', (req, res) => mailer.send(req, res));

module.exports = router;
