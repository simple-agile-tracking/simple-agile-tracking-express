const express = require('express');
const router = express.Router();

const epic = require('./epic');

router.delete('/:prodid/:id', (req, res) => epic.delete(req, res));

router.post('/:prodid', (req, res) => epic.insert(req, res));

router.put('/:prodid/:id', (req, res) => epic.update(req, res));

module.exports = router;
